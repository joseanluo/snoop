/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.foundation.controller;

import org.springframework.web.bind.annotation.*;

/**
 *  @author JoseanLuo
 *  @date 2017/12/25
 *  @email joseanluo@gmail.com
 */
@RestController
@RequestMapping("/sms")
public class FoundationController {
    @RequestMapping(value = "/{phone}", method = RequestMethod.GET)
    public String  send(@PathVariable String phone){
        System.out.println("foundation  send  sms");
        return "成功发送短信到"+phone;
    }

}
