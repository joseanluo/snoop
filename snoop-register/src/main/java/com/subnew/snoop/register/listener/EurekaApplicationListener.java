  /**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.register.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.eureka.server.event.*;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

  /**
   * application listener  service
   * @author JoseanLuo
   * @date 2017/9/7
   * @email joseanluo@gmail.com
   */
  @Slf4j
  @Configuration
  @EnableScheduling
  public class EurekaApplicationListener implements ApplicationListener {

      @Override
      public void onApplicationEvent(ApplicationEvent applicationEvent) {

          //service down  event
          if (applicationEvent instanceof EurekaInstanceCanceledEvent) {
              EurekaInstanceCanceledEvent event = (EurekaInstanceCanceledEvent) applicationEvent;
              log.error("ServiceId[{}]-AppName:[{}] is DOWN ,event time[{}],event source[{}]!!!",event.getServerId(),event.getAppName(),event.getTimestamp(),event.getSource());
          }
           //service register  Event
          if(applicationEvent  instanceof EurekaInstanceRegisteredEvent){
              EurekaInstanceRegisteredEvent event=(EurekaInstanceRegisteredEvent)applicationEvent;
              log.info("ServiceId[{}]-AppName:[{}]  register SUCCESS,detail[{}.",event.getInstanceInfo().getInstanceId(),event.getInstanceInfo().getAppName(),event.getInstanceInfo());
          }
           //health check
          if (applicationEvent instanceof EurekaInstanceRenewedEvent) {
              EurekaInstanceRenewedEvent event=(EurekaInstanceRenewedEvent)applicationEvent;
              log.info("ServiceId[{}]-AppName:[{}] status is health ,event time[{}],event source[{}].",event.getServerId(),event.getAppName(),event.getTimestamp(),event.getSource());
          }
          // EurekaRegistry is  available
          if (applicationEvent instanceof EurekaRegistryAvailableEvent) {
              EurekaRegistryAvailableEvent event=(EurekaRegistryAvailableEvent)applicationEvent;
              log.info("Registry is available,event time[{}],event source[{}].",event.getTimestamp(),event.getSource());
          }
          // EurekaServer is  started
          if (applicationEvent instanceof EurekaServerStartedEvent) {
              EurekaServerStartedEvent event=(EurekaServerStartedEvent)applicationEvent;
              log.info("EurekaServer is started,event time[{}],event source[{}].",event.getTimestamp(),event.getSource());
          }

      }
  }