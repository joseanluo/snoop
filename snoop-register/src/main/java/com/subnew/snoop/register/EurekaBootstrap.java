/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *  @author JoseanLuo
 *  @date 2017/12/25
 *  @email joseanluo@gmail.com
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(EurekaBootstrap.class,args);
    }
}
