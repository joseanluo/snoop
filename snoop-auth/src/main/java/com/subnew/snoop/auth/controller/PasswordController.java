/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.auth.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *  @author JoseanLuo
 *  @date 2017/12/25
 *  @email joseanluo@gmail.com
 */
@RestController
@RequestMapping("/password")
public class PasswordController {

    @RequestMapping(value = "/{password}", method = RequestMethod.GET)
    public String  set(@PathVariable String password){
        System.out.println("auth password  set ");
        return "成功设置密码为："+password;
    }
}
