package com.subnew.snoop.acct.rpc;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author JoseanLuo
 * @date 2017/12/25
 * @email joseanluo@gmail.com
 */
@FeignClient("snoop-auth")
@RequestMapping(value = "/password")
public interface PasswordController {

    @RequestMapping(value = "/{password}", method = RequestMethod.GET)
    public String  set(@RequestParam("password") String password);
}
