/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.acct.rpc;

/**
 *  @author JoseanLuo
 *  @date 2017/12/25
 *  @email joseanluo@gmail.com
 */

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("snoop-foundation")
@RequestMapping(value = "/sms")
public interface FoundationController {
    @RequestMapping(value = "/{phone}", method = RequestMethod.GET)
     String  send(@RequestParam("phone") String phone);
}
