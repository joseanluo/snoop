/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.acct.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *  @author JoseanLuo
 *  @date 2017/11/14
 *  @email joseanluo@gmail.com
 */
@Configuration  //标记配置类
@EnableSwagger2 //开启在线接口文档
public class SwaggerConfig {

    @Bean
    public Docket createApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                    .title("账户中心-接口文档")    //标题
                    .description("账户中心") //描述
                    .contact(new Contact("JoseanLuo","#","joseanluo@gmail.com"))//联系方式
                    .version("1.0") //版本
                    .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.subnew.snoop.acct.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}