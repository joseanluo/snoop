/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.acct.controller;

import com.subnew.snoop.acct.rpc.FoundationController;
import com.subnew.snoop.acct.rpc.PasswordController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  @author JoseanLuo
 *  @date 2017/12/25
 *  @email joseanluo@gmail.com
 */
@RestController
@RequestMapping("/acct")
@Api(value = "帐户接口", tags = "帐户接口", description = "帐户接口")
public class AcctController {

    @Autowired
    private FoundationController foundationController;

    @Autowired
    private PasswordController passwordController;

    @ApiOperation(value = "开户")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String open(@RequestParam String acctCode){
        foundationController.send("12345");
        return "成功";
    }

    @ApiOperation(value = "销户")
    @RequestMapping(value = "/{acctCode}", method = RequestMethod.DELETE)
    public String del(@PathVariable String acctCode){

        return "成功";
    }
    @ApiOperation(value = "二次开户")
    @RequestMapping(value = "/{acctCode}", method = RequestMethod.GET)
    public String  get(@PathVariable String acctCode){

        foundationController.send("12345");

        passwordController.set("123456");

        return "成功开帐户号："+acctCode;
    }
}
