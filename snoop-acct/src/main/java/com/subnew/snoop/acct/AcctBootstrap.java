/**
 * Copyright (c) 2017-2020
 * Subnew All Rights Reserved
 * Subnew is  nick name  of JoseanLuo ,This software is the confidential and proprietary information of JoseanLuo
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with JoseanLuo
 *
 * @email:joseanluo@gmail.com
 */
package com.subnew.snoop.acct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @author JoseanLuo
 * @date 2017/12/25
 * @email joseanluo@gmail.com
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class AcctBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(AcctBootstrap.class, args);
    }
}
